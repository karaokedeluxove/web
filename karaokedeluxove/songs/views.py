from django.shortcuts import render
from django.db.models import Prefetch, F, Count, Max
from django.http import JsonResponse



from .models import Artist, Song, Language, GENRES

def _filter_songs(req):
    songs = Song.objects.select_related('artist', 'language')

    if req.GET.get('artist'):
        songs = songs.filter(artist__artist__icontains=req.GET.get('artist'))

    if req.GET.get('title'):
        songs = songs.filter(title__icontains=req.GET.get('title'))

    if req.GET.get('lang'):
        songs = songs.filter(language__code=req.GET.get('lang'))

    if req.GET.get('singers'):
        songs = songs.filter(singers_count=req.GET.get('singers'))

    # doesn't matter how shitty this looks
    # this must be fucking high performance query!
    songs = songs \
        .order_by('artist__artist_ascii', 'title_ascii') \
        .values('artist__artist', 'title', 'language__language') \
        .annotate(Count('artist__artist')).annotate(Count('title')) \
        .values('title', 'duration', 'language__language', 'language__code', artist=F('artist__artist'))

    return songs


def search(req):
    max_singers = Song.objects.aggregate(Max('singers_count'))['singers_count__max']
    print(max_singers)
    return render(
        req, 'songs/search.html',
        {
            'max_singers': max_singers,
        }
    )


def flat_list(req):
    songs = _filter_songs(req).values('artist', 'title')

    return render(
        req, 'songs/flat_list.html',
        {
            'songs': songs,
        }
    )


def filter_json(req):
    songs = _filter_songs(req)

    print(songs)

    ret = []
    for song in songs:
        ret.append({
            'title': song['title'],
            'artist': song['artist'],
            'language': {
                'code': song['language__code'],
                'label': song['language__language'],
            },
        })

    return JsonResponse({'songs': ret})


def rest_languages(req):
    return JsonResponse({'languages': list(Language.objects.values('code', 'language'))})


def api_song(req, song_id=None):
    if req.method == 'GET':
        if song_id:
            song = Song.objects.get(id=song_id)
            return JsonResponse({
                'artist': song.artist.artist,
                'title': song.title,
                "duration": song.duration,
                'year': song.year,
                'genre': GENRES[song.genre] if song.genre else None,
                'language': {
                    'code': song.language.code,
                    'label': song.language.language,
                },
            })
        else:
            songs = Song.objects.all()
            ret = {'songs': []}
            for song in songs:
                ret['songs'].append({
                    'artist': song.artist.artist,
                    'title': song.title,
                    "duration": song.duration,
                    'year': song.year,
                    'genre': GENRES[song.genre] if song.genre else None,
                    'language': {
                        'code': song.language.code,
                        'label': song.language.language,
                    },
                })
            return JsonResponse(ret)
    elif req.method == 'POST':

        new_song = Song(

        )
    return



from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serializers import ArtistSerializer, SongSerializer


class ArtistViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Artist.objects.prefetch_related(Prefetch('songs',
        queryset=Song.objects.order_by('title'))).all().order_by('artist')
    serializer_class = ArtistSerializer


class SongViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Song.objects.all().order_by('artist')
    serializer_class = SongSerializer

    filterset_fields = ['artist', 'language']
