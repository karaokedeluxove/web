from django.db import models
import django_rq
import uuid
import os
import re
import subprocess

from unidecode import unidecode


FFMPEG_OUTPUT_ARGS = {
    'mp3': ['-q:a', '0', '-b:a', '320K'],
    #'m4a': ['-c:a', 'libfdk_aac', '-vn'], # needs custom ffmpeg
    'm4a': ['-c:a', 'aac', '-vn', '-b:a', '256K'],
    'ogg': ['-c:a', 'libvorbis', '-b:a', '500K'],
    'opus': ['-c:a', 'libopus', '-b:a', '510K'],
}


# from https://en.wikipedia.org/wiki/List_of_ID3v1_Genres
GENRES = [
    # 0 to 19
    'Blues',
    'Classic Rock',
    'Country',
    'Dance',
    'Disco',
    'Funk',
    'Grunge',
    'Hip-Hop',
    'Jazz',
    'Metal',
    'New Age',
    'Oldies',
    'Other',
    'Pop',
    'Rhythm and Blues',
    'Rap',
    'Reggae',
    'Rock',
    'Techno',
    'Industrial',
    # 20 to 39
    'Alternative',
    'Ska',
    'Death Metal',
    'Pranks',
    'Soundtrack',
    'Euro-Techno',
    'Ambient',
    'Trip-Hop',
    'Vocal',
    'Jazz & Funk',
    'Fusion',
    'Trance',
    'Classical',
    'Instrumental',  # really useful for karaoke ;-)
    'Acid',
    'House',
    'Game',
    'Sound clip',
    'Gospel',
    'Noise',
    # 40 to 59
    'Alternative Rock',
    'Bass',
    'Soul',
    'Punk',
    'Space',
    'Meditative',
    'Instrumental Pop',
    'Instrumental Rock',
    'Ethnic',
    'Gothic',
    'Darkwave',
    'Techno-Industrial',
    'Electronic',
    'Pop-Folk',
    'Eurodance',
    'Dream',
    'Southern Rock',
    'Comedy',
    'Cult',
    'Gangsta',
    # 60 to 79
    'Top 40',
    'Christian Rock',
    'Pop/Funk',
    'Jungle music',
    'Native US',
    'Cabaret',
    'New Wave',
    'Psychedelic',
    'Rave',
    'Showtunes',
    'Trailer',
    'Lo-Fi',
    'Tribal',
    'Acid Punk',
    'Acid Jazz',
    'Polka',
    'Retro',
    'Musical',
    'Rock \'n\' Roll',
    'Hard Rock',
    # Extension by Winamp
    # i don't fucking care right now
    'Folk',
    'Folk-Rock',
    'National Folk',
    'Swing',
    'Fast Fusion',
    'Bebop',
    'Latin',
]


def convert_file(instance, src, dst, dst_format):
    if os.path.exists(dst):
        print(f"{dst} already exists.")
        return

    try:
        ffmpeg_args = FFMPEG_OUTPUT_ARGS[dst_format]
    except KeyError:
        print(f"Destination format f{dst_format} not implemented yet")
        return

    instance.state = 3
    instance.save()

    subprocess.run(['ffmpeg', '-i', src] + ffmpeg_args + [dst])

    instance.state = 1
    instance.save()


def song_file_path(instance, filename):
    path_tree = str(instance.id)[:2]
    return f'uploads/{path_tree}/{instance.id}'


SOURCE_TYPES = {
    0: "Local",
    1: "Video",
    2: "Channel",
    3: "Playlist",
    32767: "<Unknown>",
}

SOURCE_PROVIDERS = {
    0: "Local",
    1: "YouTube",
    32767: "<Unknown>",
}

SOURCE_TYPE_ENUM = [
    (0, "Local"),
    (1, "Video"),
    (2, "Channel"),
    (3, "Playlist"),
    (32767, "Unknown"),
]

SOURCE_PROVIDER_ENUM = [
    (0, "Local"),
    (1, "YouTube"),
    (32767, "Unknown"),
]


def santize_ascii(text):
    text = unidecode(text).lower().strip()
    if text == 'p!nk':
        text = 'pink'

    text = text.replace('&', 'and')
    text = text.replace('+', 'plus')
    text = re.sub('[^\w ]', '', text)

    return text


class Source(models.Model):
    short_name = models.CharField(max_length=8)
    name = models.CharField(max_length=128)

    provider = models.PositiveIntegerField(choices=SOURCE_PROVIDER_ENUM, default=0)
    url = models.CharField(max_length=4096, null=True, blank=True)

    def __str__(self):
        return self.name


class MetadataExtractor(models.Model):
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    regexp = models.CharField(max_length=4096)


# see: http://www.loc.gov/standards/iso639-2/php/code_list.php
class Language(models.Model):
    code = models.CharField(primary_key=True, max_length=2)
    language = models.CharField(max_length=128)

    def __str__(self):
        return self.language


class Artist(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    artist = models.CharField(max_length=128)
    artist_ascii = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.artist

    def save(self, *args, **kwargs):
        self.artist_ascii = santize_ascii(self.artist)
        super().save(*args, **kwargs)


def get_storage_song_path(instance, filename):
    ext = filename.split('.')[-1]
    uuid = str(instance.id)
    return f'{uuid[0:2]}/{uuid}.{ext}'


class Song(models.Model):
    GENRES_ENUM = [(None, "<Unknown>")] + [(i, v) for i, v in enumerate(GENRES)]

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    artist = models.ForeignKey(Artist, related_name='songs', on_delete=models.CASCADE)
    title = models.CharField(max_length=128)
    title_ascii = models.CharField(max_length=128)
    duration = models.IntegerField(help_text="(seconds)", null=True, blank=True)
    genre = models.SmallIntegerField(choices=GENRES_ENUM, null=True, blank=True)
    year = models.SmallIntegerField(null=True, blank=True)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    singers_count = models.PositiveSmallIntegerField(default=1)

    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    url = models.CharField(max_length=4096, null=True, blank=True)

    file = models.FileField(upload_to=get_storage_song_path, null=True, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.artist} — {self.title}"

    def save(self, *args, **kwargs):
        self.title_ascii = santize_ascii(self.title)
        super().save(*args, **kwargs)

        if self.file.name:
            from google.cloud import storage
            sc = storage.Client()
            bucket = sc.get_bucket('karaokedeluxove-files')
            blob = bucket.get_blob(self.file.name)

            ext = self.file.name.split('.')[-1]
            blob.content_language = self.language.code
            fname = f'{self.artist} - {self.title}' if not self.source else f'{self.artist} - {self.title} [{self.source.name}]'
            blob.content_disposition = f'attachment; filename="{fname}.{ext}"'
            blob.patch()

    class Meta:
        unique_together = [['artist', 'title_ascii', 'source', 'singers_count']]


class SongRecord(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    singer = models.CharField(max_length=128)
    song = models.ForeignKey(Song, on_delete=models.PROTECT)
    base_file = models.FileField(upload_to=song_file_path)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.song.artist} - {self.song.title} (sung by {self.singer})"

    def save(self, *args, **kwargs):
        super(SongRecord, self).save(*args, **kwargs)

        for audio_format in ['mp3', 'm4a', 'ogg', 'opus']:
            song_file = SongRecordFile(song_id=self, format=audio_format, state=0)
            song_file.save()

            result = django_rq.enqueue(
                convert_file, song_file, self.base_file.name, f'{self.base_file.name}.{audio_format}', audio_format)

            song_file.state = 2
            song_file.save()
            print(result)


class SongRecordFile(models.Model):
    FILE_FORMAT_CHOICES = [
        ('mp3', 'MPEG-1 Audio Layer 3'),
        ('m4a', 'MPEG-4 Audio Layer'),
        ('ogg', 'Ogg Vorbis'),
        ('opus', 'Opus'),
    ]
    STATE_CHOICES = [
        (0, 'Unavailable'),
        (1, 'Available'),
        (2, 'Queued'),
        (3, 'Converting'),
        (4, 'Error'),
    ]

    song_id = models.ForeignKey(SongRecord, on_delete=models.CASCADE)
    format = models.CharField(max_length=8, choices=FILE_FORMAT_CHOICES, editable=False)
    state = models.SmallIntegerField(choices=STATE_CHOICES, editable=False)

    def url(self):
        if self.state == 1:
            return f'{self.song_id.base_file.url}.{self.format}'

    class Meta:
        unique_together = ('song_id', 'format',)
