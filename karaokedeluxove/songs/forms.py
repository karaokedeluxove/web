from django import forms

from .models import Song


class SongAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(SongAdminForm, self).__init__(*args, **kwargs)
        sorted_genres = sorted(Song.GENRES_ENUM, key=lambda x: x[1])
        self.fields['genre'].choices = sorted_genres
