from django.contrib import admin
from django.db.models import Count
from django.utils.html import format_html
from .models import *
from .forms import *

import time


class UsedGenresFilter(admin.SimpleListFilter):
    title = 'genre'
    parameter_name = 'genre'

    def lookups(self, request, model_admin):
        used_genres_list = Song.objects.values('genre').annotate(Count('genre'))
        ret = []
        for genre in used_genres_list:
            if not genre['genre']:
                continue

            print(genre)
            ret.append((
                genre['genre'],
                "{} ({})".format(GENRES[genre['genre']], genre['genre__count'])
            ))
        return sorted(ret, key=lambda x: x[1])

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(genre=self.value())
        return queryset


class LanguageFilter(admin.SimpleListFilter):
    title = 'language'
    parameter_name = 'language'

    def lookups(self, request, model_admin):
        langs = Language.objects.all()
        langs_use_count = Song.objects.values('language__code').annotate(Count('language__code'))
        langs_use_dict = {}

        for lang in langs_use_count:
            langs_use_dict[lang['language__code']] = lang['language__code__count']

        ret = []
        for lang in langs:
            if lang.code in langs_use_dict:
                lang_count = langs_use_dict[lang.code]
            else:
                lang_count = 0

            ret.append((
                lang.code,
                "{} ({})".format(lang.language, lang_count)
            ))
        return sorted(ret, key=lambda x: x[1])

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(language=self.value())
        return queryset


class MetadataExtractorInlineAdmin(admin.TabularInline):
    model = MetadataExtractor
    extra = 0


class SongInlineAdmin(admin.TabularInline):
    model = Song
    extra = 0


class SourceAdmin(admin.ModelAdmin):
    list_display = ['short_name', 'name', 'provider', 'songs_count']
    list_display_links = ['name']
    inlines = [MetadataExtractorInlineAdmin]
    empty_value_display = '-empty-'

    def get_queryset(self, request):
        x = Source.objects.annotate(songs_count=Count('song'))
        return x

    def songs_count(self, obj):
        return obj.songs_count


class ArtistAdmin(admin.ModelAdmin):
    list_display = ['artist', 'songs_count']
    search_fields = ['artist', 'artist_ascii']
    inlines = [SongInlineAdmin]
    exclude = ['artist_ascii']

    def get_queryset(self, request):
        return Artist.objects.annotate(songs_count=Count('songs'))

    def songs_count(self, obj):
        return obj.songs_count


class LanguageAdmin(admin.ModelAdmin):
    list_display = ['code', 'language']


class SongAdmin(admin.ModelAdmin):
    form = SongAdminForm

    list_display = ['artist', 'title', 'duration', 'singers_count', 'language', 'genre', 'source']
    list_display_links = ['title']
    list_filter = ['singers_count', 'source', LanguageFilter, UsedGenresFilter]
    search_fields = ['artist__artist', 'artist__artist_ascii', 'title', 'title_ascii']

    ordering = ['artist__artist_ascii', 'title_ascii']

    fieldsets = (
        (None, {
            'fields': (
                ('file',),
                ('title', 'artist'),
                ('duration', 'year', 'genre'),
                ('language', 'singers_count'),
            ),
        }),
        ("File source", {
            'fields': (
                'source',
                'url'
            ),
        }),
    )

    def duration(self, instance):
        if instance.duration:
            return time.strftime("%M:%S", time.gmtime(instance.duration))
        return None


class SongRecordFileAdmin(admin.TabularInline):
    model = SongRecordFile
    readonly_fields = ['format', 'state', 'clickable_url']
    fields = ['format', 'state', 'clickable_url']
    extra = 0
    max_num = 0

    def clickable_url(self, instance):
        if instance.url():
            return format_html(
                '<a href="{0}" target="_blank">{1}</a>',
                instance.url(),
                instance.url(),
            )
        else:
            return ""


class SongRecordAdmin(admin.ModelAdmin):
    readonly_fields = ['created_at']
    inlines = [SongRecordFileAdmin]


admin.site.register(Source, SourceAdmin)
admin.site.register(Artist, ArtistAdmin)
admin.site.register(Language, LanguageAdmin)
admin.site.register(Song, SongAdmin)

#admin.site.register(SongFile, SongFileAdmin)
admin.site.register(SongRecord, SongRecordAdmin)
