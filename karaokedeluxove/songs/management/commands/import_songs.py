from django.core.management.base import BaseCommand, CommandError

import os
import re

from ...models import Source, Artist, Song, Language


class Command(BaseCommand):
    help = "Imports karaoke songs"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            '--base-path',
        )
        parser.add_argument(
            '--source-name'
        )

    def handle(self, **options):
        try:
            source = Source.objects.get(name=options['source_name'])
        except Exception:
            source = None

        lang_en = Language.objects.get(code='en')

        for path, _, filenames in os.walk(options['base_path']):
            for filename in filenames:
                print(f"Importing {path}/{filename}")
                m = re.match("^(?P<artist>.*?) - (?P<title>.*?)\.(kfn|zip|mp4)$", filename)

                if not m:
                    continue

                artist, artist_created = Artist.objects.get_or_create(artist=m.group('artist'))

                new_song = Song(
                    artist=artist,
                    title=m.group('title'),
                    language=lang_en,
                    source=source,
                )
                new_song.save()
                pass
