from django.core.exceptions import ObjectDoesNotExist
from django.utils.encoding import smart_text
from rest_framework import serializers

from .models import Artist, Song, Language, Source


class CreatableSlugRelatedField(serializers.SlugRelatedField):
    def to_internal_value(self, data):
        try:
            return self.get_queryset().get_or_create(**{self.slug_field: data})[0]
        except ObjectDoesNotExist:
            self.fail('does_not_exist', slug_name=self.slug_field, value=smart_text(data))
        except (TypeError, ValueError):
            self.fail('invalid')


class ArtistSerializer(serializers.HyperlinkedModelSerializer):
    songs = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='title',
        #queryset=Song.objects.all().order_by('title')
     )

    class Meta:
        model = Artist
        fields = ['id', 'artist', 'songs']


class SongSerializer(serializers.HyperlinkedModelSerializer):
    artist = CreatableSlugRelatedField(
        slug_field='artist',
        queryset=Artist.objects.all().order_by('artist'),
    )
    language = serializers.SlugRelatedField(
        slug_field='code',
        queryset=Language.objects.all().order_by('language'),
    )
    source = serializers.SlugRelatedField(
        slug_field='name',
        queryset=Source.objects.all().order_by('name'),
        default=None,
    )

    class Meta:
        model = Song
        lookup_field = ['artist', 'genre', 'year', 'language', 'singers_count']
        search_fields = ['artist', 'artist_ascii', 'title', 'title_ascii']
        fields = ['id', 'artist', 'title', 'duration', 'genre', 'year', 'language', 'singers_count', 'source']

