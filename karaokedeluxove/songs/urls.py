from django.urls import include, path, re_path
from rest_framework import routers

from . import views

app_name = 'songs'

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'artists', views.ArtistViewSet)
router.register(r'songs', views.SongViewSet)

urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('',
            views.search, name='search'),
    path('flat.html',
            views.flat_list, name='flat_list'),
    path('filter.json',
         views.filter_json, name='filter_json'),
    path('languages.json',
         views.rest_languages, name='rest_languages'),
    re_path('api/songs(/(?P<song_id>.*))?$',
            views.api_song, name='api_song'),
]

