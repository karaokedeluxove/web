from django.utils.deconstruct import deconstructible
from storages.backends.gcloud import GoogleCloudStorage


@deconstructible
class MyGCPS(GoogleCloudStorage):
    def get_valid_name(self, name):
        return name.replace('/', '-')
