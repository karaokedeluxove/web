FROM python:3.7-alpine

COPY karaokedeluxove /srv
COPY requirements.txt /requirements.txt
COPY entrypoint.sh /entrypoint

RUN \
    apk update && apk add postgresql-dev gcc python3-dev musl-dev && \
    pip install --no-cache-dir -r /requirements.txt

WORKDIR /srv
ENTRYPOINT ["/entrypoint"]
