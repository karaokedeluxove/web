#!/usr/bin/env sh

echo -n "\033[1;32m * \033[1;37mRunning migrations...\033[0m"
./manage.py migrate
echo -n "\033[1;32m * \033[1;37mStarting application...\033[0m"
exec ./manage.py runserver 0.0.0.0:8000
#exec gunicorn karaokedeluxove.wsgi
